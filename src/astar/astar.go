package astar

import (
	"world"
	"math"
)

type Node struct {
	parent     *Node
	value      int
	point      world.Point
	startCost  int
	finishCost int
}

func FindPath(w world.World) []world.Point {
	return calculatePath(w, manhattanDistance)
}

func calculatePath (w world.World, distanceMethod func(start, finish world.Point) int) []world.Point {
	myPathStart := getNode(w, nil, w.StartPoint)
	myPathEnd := getNode(w, nil, w.FinishPoint)

	AStar := make(map[int]bool)

	openNodes := []Node{myPathStart}
	closedNodes := []Node{}

	result := []world.Point{}

	var (
		max int
		min int
	)

	for length := len(openNodes); length != 0; {
		max = w.Row * w.Column
		min = -1

		for i, v := range openNodes {
			if v.startCost < max {
				max = v.startCost
				min = i
			}
		}

		myNode := openNodes[min]
		openNodes = append(openNodes[:min], openNodes[min + 1:]...)

		if myNode.value == myPathEnd.value {

			closedNodes = append(closedNodes, myNode)
			myPath := myNode

			for {
				result = append(result, world.Point{myPath.point.X, myPath.point.Y})

				if myPath.parent != nil {
					myPath = *myPath.parent
				} else {
					break
				}
			}

			AStar = make(map[int]bool)
			closedNodes = []Node{}
			openNodes = []Node{}

			reversArray(result)

		} else {
			myNeighbours := getNeighboursForPoint(w, myNode.point, true)

			for i, j := 0, len(myNeighbours); i < j; i++ {
				myPath := getNode(w, &myNode, myNeighbours[i])

				if _, ok := AStar[myPath.value]; !ok {
					myPath.finishCost = myNode.finishCost + distanceMethod(myNeighbours[i], myNode.point)
					myPath.startCost = myPath.finishCost + distanceMethod(myNeighbours[i], myPathEnd.point)

					openNodes = append(openNodes, myPath)

					AStar[myPath.value] = true
				}
			}

			closedNodes = append(closedNodes, myNode)
		}

		length = len(openNodes)
	}

	return result
}

func getNode (w world.World, parent *Node, p world.Point) Node {
	return Node{
		parent,
		p.X + p.Y * w.Column,
		p,
		0,
		0,
	}
}

func reversArray (arr []world.Point) []world.Point {
	for i, j := 0, len(arr) - 1; i < j; i, j = i + 1, j - 1 {
		arr[i], arr[j] = arr[j], arr[i]
	}

	return arr
}

func getNeighboursForPoint (w world.World, p world.Point, isTread bool) []world.Point {
	var result []world.Point

	if isTread {
		ch := make(chan []world.Point, 4)

		for i := 0; i <= 3; i++ {
			go getWithTread(w, p, i, ch)
		}

		for i := 0; i <= 3; i++ {
			result = append(result, <- ch...)
		}
	} else {
		result = getWithoutTread(w, p)
	}


	return result
}

func getWithoutTread(w world.World, p world.Point) []world.Point {
	N, S, E, W := p.Y - 1, p.Y + 1, p.X - 1, p.X + 1
	result := make([]world.Point, 0)

	myN := N > -1 && w.CanWalk(world.Point{p.X, N})
	myS := S < w.Row && w.CanWalk(world.Point{p.X, S})
	myE := E < w.Column && w.CanWalk(world.Point{E, p.Y})
	myW := W > -1 && w.CanWalk(world.Point{W, p.Y})

	if myN {
		result = append(result, world.Point{p.X, N})

		//For diagonalDistance
		//if myE && w.canWalk(Point{E, N}) {
		//	result = append(result, Point{E, N})
		//}
		//
		//if myW && w.canWalk(Point{W, N}) {
		//	result = append(result, Point{W, N})
		//}
	}

	if myE {
		result = append(result, world.Point{E, p.Y})
	}

	if myS {
		result = append(result, world.Point{p.X, S})

		//For diagonalDistance
		//if myE && w.canWalk(Point{E, S}) {
		//	result = append(result, Point{E, S})
		//}
		//
		//if myW && w.canWalk(Point{W, S}) {
		//	result = append(result, Point{W, S})
		//}
	}

	if myW {
		result = append(result, world.Point{W, p.Y})
	}

	return result
}

func getWithTread(w world.World, p world.Point, direction int, ch chan <- []world.Point) {
	switch direction{
	case 0:
		side := p.Y - 1
		point := world.Point{p.X, side}
		isEmpty := side > -1 && w.CanWalk(point)
		if isEmpty {
			ch <- []world.Point{point}
		} else {
			ch <- []world.Point{}
		}
	case 1:
		side := p.Y + 1
		point := world.Point{p.X, side}
		isEmpty := side < w.Row && w.CanWalk(point)
		if isEmpty {
			ch <- []world.Point{point}
		} else {
			ch <- []world.Point{}
		}
	case 2:
		side := p.X - 1
		point := world.Point{side, p.Y}
		isEmpty := side > -1 && w.CanWalk(point)
		if isEmpty {
			ch <- []world.Point{point}
		} else {
			ch <- []world.Point{}
		}
	case 3:
		side := p.X + 1
		point := world.Point{side, p.Y}
		isEmpty := side < w.Column && w.CanWalk(point)
		if isEmpty {
			ch <- []world.Point{point}
		} else {
			ch <- []world.Point{}
		}
	default:
		ch <- []world.Point{}
	}
}



func manhattanDistance (start, finish world.Point) int {
	return int(math.Abs(float64(start.X - finish.X)) + math.Abs(float64(start.Y - finish.Y)))
}

func diagonalDistance (start, finish world.Point) int {
	return int(math.Max(math.Abs(float64(start.X - finish.X)), math.Abs(float64(start.Y - finish.Y))))
}

func euclideanDistance (start, finish world.Point) int {
	return int(math.Sqrt(math.Pow(float64(start.X - finish.X), 2) + math.Pow(float64(start.Y - finish.Y), 2)))
}
