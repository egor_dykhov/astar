package astar

import (
	"world"
	"testing"
)

var (
	MyWorld1 world.World
	MyWorld2 world.World
	MyWorld3 world.World
	result []world.Point
)

func init () {
	MyWorld1.Init(50, 50)
	MyWorld2.Init(500, 500)
	MyWorld3.Init(1000, 1000)
}

func Benchmark50(b *testing.B) {
	var r []world.Point

	for n := 0; n < b.N; n++ {
		MyWorld1.CreateWorld()
		r = FindPath(MyWorld1)
	}

	result = r
}

func Benchmark500(b *testing.B) {
	var r []world.Point

	for n := 0; n < b.N; n++ {
		MyWorld2.CreateWorld()
		r = FindPath(MyWorld2)
	}

	result = r
}

func Benchmark1000(b *testing.B) {
	var r []world.Point

	for n := 0; n < b.N; n++ {
		MyWorld3.CreateWorld()
		r = FindPath(MyWorld3)
	}

	result = r
}