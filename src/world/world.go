// " " - space in the world
// # - walls
// S - start point
// F - finish point
// . - trace
package world

import (
	"fmt"
	"math/rand"
	"time"
	"github.com/fatih/color"
)

type Point struct {
	X int
	Y int
}



type World struct {
	Field       [][]string
	StartPoint  Point
	FinishPoint Point
	CurrentPath []Point
	Row         int
	Column      int
}

func (w *World) Init (row, column int) {
	w.Row = row
	w.Column = column

	w.Field = make([][]string, row)

	for i := range w.Field {
		w.Field[i] = make([]string, column)

		for j := range w.Field[i] {
			w.Field[i][j] = " "
		}
	}

	rand.Seed(int64(time.Now().Nanosecond()))
}

func (w *World) Show () {
	trace := color.New(color.FgHiWhite, color.BgGreen)
	wall := color.New(color.FgRed, color.BgRed)
	points := color.New(color.FgHiYellow, color.BgGreen)
	space := color.New(color.BgGreen)

	for _, v := range w.Field {
		for _, val := range v {
			rand.Seed(int64(time.Now().Nanosecond()))

			if val == "#" {
				wall.Print(val)
			} else if val == "." {
				trace.Print(val)
			} else if val == "S" || val == "F" {
				points.Print(val)
			} else {
				space.Print(val)
			}
		}
		fmt.Println()
	}
}

func (w *World) CreateWorld () {
	for i, v := range w.Field {
		for j := range v {
			if rand.Float32() > 0.7 {
				w.Field[i][j] = "#"
			}
		}
	}

	w.StartPoint.X = int(rand.Intn(w.Column))
	w.StartPoint.Y = int(rand.Intn(w.Row))

	w.FinishPoint.X = int(rand.Float32() * float32(w.Column))
	w.FinishPoint.Y = int(rand.Float32() * float32(w.Row))

	w.Field[w.StartPoint.Y][w.StartPoint.X] = "S"
	w.Field[w.FinishPoint.Y][w.FinishPoint.X] = "F"
}

func (w *World) SetPoints(arr []Point) {
	for _, v := range arr {
		w.Field[v.Y][v.X] = "."
	}

	w.Field[w.StartPoint.Y][w.StartPoint.X] = "S"
	w.Field[w.FinishPoint.Y][w.FinishPoint.X] = "F"
}

func (w World)CanWalk (p Point) bool {
	if p.Y >= 0 && p.Y < w.Row && p.X >= 0 && p.X < w.Column {
		if w.Field[p.Y][p.X] == " " || w.Field[p.Y][p.X] == "F" {
			return true
		}
	}

	return false
}