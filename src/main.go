package main

import (
	"world"
	"astar"
)

var MyWorld world.World

func init () {
	MyWorld.Init(50, 70)
}

func main () {
	MyWorld.CreateWorld()
	MyWorld.SetPoints(astar.FindPath(MyWorld))
	MyWorld.Show()
}